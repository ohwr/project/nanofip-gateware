--_________________________________________________________________________________________________
--                                                                                                |
--                                         |The nanoFIP|                                          |
--                                                                                                |
--                                         CERN,BE/CO-HT                                          |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                             wf_crc                                             |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         wf_crc.vhd                                                                        |
--                                                                                                |
-- Description  The unit creates the modules for:                                                 |
--                o the generation of the CRC of serial data,                                     |
--                o the verification of an incoming CRC syndrome.                                 |
--              The unit is instantiated in both the wf_fd_transmitter, for the generation of the |
--              FCS field of produced RP_DAT frames, and the wf_fd_receiver for the validation of |
--              of an incoming ID_DAT or consumed RP_DAT frame.                                   |
--                                                                                                |
-- Authors      Pablo Alvarez Sanchez (Pablo.Alvarez.Sanchez@cern.ch)                             |
-- Date         23/02/2011                                                                        |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------



--=================================================================================================
--                                      Libraries & Packages
--=================================================================================================

-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all; -- std_logic definitions
use IEEE.NUMERIC_STD.all;    -- conversion functions
-- Specific library
library work;
use work.WF_PACKAGE.all;     -- definitions of types, constants, entities


--=================================================================================================
--                                 Entity declaration for wf_crc
--=================================================================================================
entity wf_crc is port(
  -- INPUTS
    -- nanoFIP User Interface, General signals
    uclk_i             : in std_logic;              -- 40 MHz clock

    -- Signal from the wf_reset_unit
    nfip_rst_i         : in std_logic;              -- nanoFIP internal reset

    -- Signals from the wf_rx_deserializer/ wf_tx_serializer units
    data_bit_i         : in std_logic;              -- incoming data bit stream
    data_bit_ready_p_i : in std_logic;              -- indicates the sampling moment of data_bit_i
    start_crc_p_i      : in std_logic;              -- beginning of the CRC calculation


  -- OUTPUTS
    -- Signal to the wf_rx_deserializer unit
    crc_ok_p_o         : out std_logic;             -- signals a correct received CRC syndrome

    -- Signal to the wf_tx_serializer unit
    crc_o              : out  std_logic_vector (c_CRC_POLY_LGTH-1 downto 0)); -- calculated CRC

end entity wf_crc;


--=================================================================================================
--                                    architecture declaration
--=================================================================================================
architecture rtl of wf_crc is

  signal s_q, s_q_nx : std_logic_vector (c_CRC_POLY_LGTH - 1 downto 0);


--=================================================================================================
--                                       architecture begin
--=================================================================================================
begin

---------------------------------------------------------------------------------------------------
--                                         CRC Calculation                                       --
---------------------------------------------------------------------------------------------------

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
-- The Gen_16_bit_Register_and_Interconnections generator, follows the scheme of figure A.1
-- of the Annex A 61158-4-7 IEC:2007 and constructs a register of 16 master-slave flip-flops which
-- are interconnected as a linear feedback shift register.

  Generate_16_bit_Register_and_Interconnections:

    s_q_nx(0)   <= data_bit_i xor s_q(s_q'left);

    G: for I in 1 to c_CRC_GENER_POLY'left generate
      s_q_nx(I) <= s_q(I-1) xor (c_CRC_GENER_POLY(I) and (data_bit_i xor s_q(s_q'left)));
    end generate;


--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
-- Synchronous process CRC_calculation: the process "moves" the shift register described
-- above, for the calculation of the CRC.

  CRC_calculation: process (uclk_i)
  begin
    if rising_edge (uclk_i) then

      if nfip_rst_i = '1' then
        s_q     <= (others => '0');

      else

        if start_crc_p_i = '1' then
          s_q   <= (others => '1');          -- register initialization
                                             -- (initially preset, according to the Annex)

        elsif data_bit_ready_p_i = '1' then  -- new bit to be considered for the CRC calculation
          s_q   <= s_q_nx;                   -- data propagation

        end if;
      end if;
    end if;
  end process;

  --  --  --  --  --
  crc_o         <= not s_q;



---------------------------------------------------------------------------------------------------
--                                       CRC Verification                                        --
---------------------------------------------------------------------------------------------------

-- During reception, the CRC is being calculated as data is arriving (same as in the transmission)
-- and at the same time it is being compared to the predefined c_CRC_VERIF_POLY. When the CRC
-- calculated from the received data matches the c_CRC_VERIF_POLY, it is implied that a correct CRC
-- word has been received for the preceded data and the signal crc_ok_p_o gives a 1 uclk-wide pulse.

  crc_ok_p_o <= data_bit_ready_p_i when s_q = not c_CRC_VERIF_POLY else '0';



end architecture rtl;
--=================================================================================================
--                                        architecture end
--=================================================================================================
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------